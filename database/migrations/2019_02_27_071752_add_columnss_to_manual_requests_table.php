<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnssToManualRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('manual_requests', function (Blueprint $table) {
            $table->integer('fixed_fare');
            $table->float('min_price')->default(0);
            $table->float('base_price')->default(0);
            $table->float('time_price')->default(0);
            $table->float('distance_price')->default(0);
            $table->float('tax_price')->default(0);
            $table->float('booking_fee')->default(0);
            $table->string('distance_unit');

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('manual_requests', function (Blueprint $table) {
            $table->dropColumn('fixed_fare');
            $table->dropColumn('min_price');
            $table->dropColumn('base_price');
            $table->dropColumn('time_price');
            $table->dropColumn('distance_price');
            $table->dropColumn('tax_price');
            $table->dropColumn('booking_fee');
            $table->dropColumn('distance_unit');

        });

    }
}


