<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateManualRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('manual_requests', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('provider_id');
            $table->double('s_latitude',15,8);
            $table->double('s_longitude',15,8);
            $table->double('d_latitude',15,8);
            $table->double('d_longitude',15,8);
            $table->string('s_address');
            $table->string('d_address');
            $table->integer('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('manual_requests');
    }
}
