<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToManualRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('manual_requests', function (Blueprint $table) {
            $table->float('time')->default(0);
            $table->float('distance')->default(0);
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('manual_requests', function (Blueprint $table) {
            $table->dropColumn('time');
            $table->dropColumn('distance');
        });

    }
}
