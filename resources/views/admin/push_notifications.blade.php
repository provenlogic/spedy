@extends('layouts.admin')
@if(isset($name))
@section('title', 'Push Notifications')
@else
@section('title', 'Push Notifications')
@endif
@if(isset($name))
@section('content-header', 'Push Notifications')
@else
@section('content-header', 'Push Notifications')
@endif
@section('breadcrumb')
<li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i>{{tr('home')}}</a></li>
<li><a href="{{route('admin.corporates')}}"><i class="fa fa-users"></i> 'Push Notifications'</a></li>
@if(isset($name))
<li class="active">{{tr('edit_corporate')}}</li>
@else
<li class="active">{{tr('add_corporate')}}</li>
@endif
@endsection
@section('content')
@include('notification.notify')
<div class="row">
    <div class="col-md-12">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Send Push Notifications To Users</h3>
            </div>
            <form class="form-horizontal bordered-group" action="{{route('admin.mass_push_notification_send')}}" method="POST" role="form">
                <div class="box-body">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Notification Title</label>
                        <div class="col-sm-8">
                            <input type="text" name="push_title" value="{{ old('push_title') }}" required class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Enter the Message</label>
                        <div class="col-sm-8">
                            <textarea name="push_message" required class="form-control" rows="3"></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Select Users</label>
                        <div class="col-sm-8">
                            <div class="checkbox">
                                <strong id="userCount" style="color:green;">  You have selected <span id="userSize">0</span> user(s).</strong>
                            </div>
                            <div class="checkbox">
                                <label><input type="checkbox" id="onCheckAll">All</label>
                            </div>
                            <select class="form-control" required id="allUsers" name="numbers[]" multiple="multiple" style="height:200px;">
                                @foreach($result as $taken)
                                <option value="{{ $taken->id }}">{{ $taken->first_name }} {{ $taken->last_name }} ({{ $taken->email }})</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="box-footer">
                        <input type="hidden" name="type" value="users">
                        <button type="submit" class="btn btn-success pull-right">Send</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

	<div class="col-md-12">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Send Push Notifications To Drivers</h3>
            </div>
            <form class="form-horizontal bordered-group" action="{{route('admin.mass_push_notification_send')}}" method="POST" role="form">
                <div class="box-body">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Notification Title</label>
                        <div class="col-sm-8">
                            <input type="text" name="push_title" value="{{ old('push_title') }}" required class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Enter the Message</label>
                        <div class="col-sm-8">
                            <textarea name="push_message" required class="form-control" rows="3"></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Select Drivers</label>
                        <div class="col-sm-8">
                            <div class="checkbox">
                                <strong id="userCount-provider" style="color:green;">  You have selected <span id="userSize-provider">0</span> driver(s).</strong>
                            </div>
                            <div class="checkbox">
                                <label><input type="checkbox" id="onCheckAll-provider">All</label>
                            </div>
                            <select class="form-control" required id="allUsers-provider" name="numbers[]" multiple="multiple" style="height:200px;">
                                @foreach($providers as $taken)
                                <option value="{{ $taken->id }}">{{ $taken->first_name }} {{ $taken->last_name }} ({{ $taken->email }})</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="box-footer">
                        <input type="hidden" name="type" value="providers">
                        <button type="submit" class="btn btn-success pull-right">Send</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

</div>
<script src="{{asset('admin-css/plugins/jQuery/jQuery-2.2.0.min.js')}}"></script>
<script type="text/javascript">
    $('#onCheckAll').change(function() {
    	  	
    	if($(this).is(":checked")) {
    		$('#allUsers option').prop('selected', true);
    		var size = $('#allUsers option:selected').size();
    		$("#userSize").html(size);
    	} else {
    		$('#allUsers option').prop('selected', false);
    	}      
    });
       
    $('#allUsers').change(function(){
    	$('#onCheckAll').prop('checked', false)
    	var size = $('#allUsers option:selected').size();
    	$("#userSize").html(size);
	});
	

	$('#onCheckAll-provider').change(function() {
    	  	
    	if($(this).is(":checked")) {
    		$('#allUsers-provider option').prop('selected', true);
    		var size = $('#allUsers-provider option:selected').size();
    		$("#userSize-provider").html(size);
    	} else {
    		$('#allUsers-provider option').prop('selected', false);
    	}      
    });
       
    $('#allUsers-provider').change(function(){
    	$('#onCheckAll-provider').prop('checked', false)
    	var size = $('#allUsers-provider option:selected').size();
    	$("#userSize-provider").html(size);
    });
       
</script>
@endsection