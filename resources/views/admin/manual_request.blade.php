<?php 
  use App\Admin; 
  $is_permitted_user = Admin::find(Auth::guard('admin')->user()->id);
?>
@extends('layouts.admin')

@if(isset($name))
  @section('title', tr('view_history'))
@else
  @section('title', tr('requests'))
@endif

@if(isset($name))
  @section('content-header', tr('view_history'))
@else
  @section('content-header', 'Manual Requests Management')
@endif


@section('breadcrumb')
    <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i>{{tr('home')}}</a></li>
    @if(isset($name))
      <li><a href="{{route('admin.users')}}"><i class="fa fa-user"></i> {{tr('users')}}</a></li>
      <li class="active"><i class="fa fa-university"></i> {{tr('view_history')}}</li>
    @else

      <li class="active"><i class="fa fa-university"></i> Ride Requests Management</li>
    @endif

@endsection

@section('content')

    @include('notification.notify')

<div class="row">
  <div class="col-xs-12">

      <a class="btn btn-primary " href="{{url('/')}}/admin/manualRideExcel" style="margin-left:10px">Export Excel</a>
      <a class="btn btn-primary " href="{{url('/')}}/admin/manualRidePdf" style="margin-left:10px">Export PDF</a>
      <!-- <button class="btn btn-primary " onclick="$('.example1').tableExport({type:'excel',escape:'false'});" style="margin-left:10px">Export</button> -->
<br><br>



    <div class="box box-info">
       <div class="box-header">
        <div class="map_content">
            <p class="lead para_mid">
               This screen gives you a Gods view of all the Manual Requests you have got over time. Can come in very handy to analyze & plan things for your business.

            </p>
             <p class="lead para_mid">
              More Data. More Power!
            </p>
          </div>
          </div>
      <div class="box-body">

      	@if(count($requests) > 0)

          	<table id="example1" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>ID</th>
                  <th class="min">Driver Name</th>
                  <th class="min">Date and Time</th>
                  <th>Status</th>
                  <th>Price</th>
                  <th>Payment Mode</th>
                  <th>Action</th>

                  </tr>
              </thead>
              <tbody>
              @foreach($requests as $index => $requestss)
              <tr>
                  <td>{{$index + 1}}</td>
                  <td>@if($requestss->first_name){{$requestss->first_name . " " . $requestss->last_name}} @else - @endif</td>
                  <td>{{$requestss->date}}</td>
                  <td>@if($requestss->status == 0)
                            Pickup
                      @elseif($requestss->status == 1)
                            Pickup
                      @elseif($requestss->status == 2)
                            Trip Started

                      @elseif($requestss->status == 3)
                            Trip Completed
                        @elseif($requestss->status == 4)
                            Trip Cancelled
      
                        @endif
                    </td>
                    <td> {{$requestss->total}} </td>
                    <td>
                      Cash
                    </td>
                    <td>

                        <div class="input-group-btn">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">Action
                              <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu">
                              <li>
                                <a href="{{route('admin.view.manual.request', array('id' => $requestss->id))}}">View Request</a>
                              </li>
                              <li>
                                <a href="{{route('admin.cancel.manual', array('id' => $requestss->id))}}">Cancel Request</a>
                              </li>

    
                          
                            </ul>
                          </div>
  
                    </td>  
              </tr>
              @endforeach
              </tbody>
		</table>
	@else
		<h3 class="no-result">{{tr('no_data_found')}}</h3>
	@endif
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="show_providers_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Provider List</h4>
      </div>
      <div class="modal-body">
       <p id="prov_message">Message</p>
        <div class="table-responsive">
          <div id="here_table"></div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
function showProviders(request_id)
 { 
  //alert(request_id);return;
//  $("#show_providers_modal").modal("show");  
// alert(request_id);return;
// if($("#hours_err_msg").val() == 1){
//   alert('please enter valid hours');
//   return;
// }  
            var type = 're_assign';
            var request_id = request_id;
            //var user_id = $("#user_id").val();
            //alert(user_id);
            //var uniq_id = $('#uniq_id').val();
            //var service_id = $('#service_id').val();
            //var total = $('#total').val();
            //var first_name = $('#first_name').val();
            //var last_name = $('#last_name').val();
            //var email = $('#email').val();
            // var s_latitude = $('#s_latitude').val();
            // var s_longitude = $('#s_longitude').val();
            // var d_latitude = $('#d_latitude').val();
            // var d_longitude = $('#d_longitude').val();
            // var s_address = $('#origin-input').val();
            // var d_address = $('#destination-input').val();
            // var request_status_type = $('#request_status_type').val();
            // var hourly_package_id = $('#hourly_package_id').val();
            // var airport_price_id = $('#airport_price_id').val();
         //alert(request_status_type);   

   //var dataString = 'uniq_id='+uniq_id+'&service_id='+service_id+'&total='+total+'&first_name='+first_name+'&last_name='+last_name+'&email='+email+'&s_latitude='+s_latitude+'&s_longitude='+s_longitude+'&d_latitude='+d_latitude+'&d_longitude='+d_longitude+'&s_address='+s_address+'&d_address='+d_address+'&request_status_type='+request_status_type+'&hourly_package_id='+hourly_package_id+'&airport_price_id='+airport_price_id;
      
    //var dataString = 'type='+type+'&request_id='+request_id+'&user_id='+user_id;
    var dataString = 'type='+type+'&request_id='+request_id;

//alert(dataString);
   $.ajax({
   type: "POST",
   url : "{{route('admin.find_providers')}}",
   data: dataString, 
   success : function(data){
     //alert(data);return;
      var json = $.parseJSON(data);
      console.log(json);
     $('#here_table').html("  ");
     if (json.success == "true" || json.success == 1 && json.success != "") {
        $('#prov_message').html("Provider Details :");  
        var trHTML = '';
        var sl_no = 0;
        $.each(json, function (i, item) {

          // if (i == "request_status_type") {
          //   var request_status_type = item;
          //   $("#request_status_type").val(request_status_type);
          // };
          if(i == "providers"){
             var providers = $.parseJSON(item);
             //$('#providers').val(providers);
             trHTML += "<table class='table table-striped' style='margin-bottom:0;'>";
             $.each(providers, function (j, provider) {
              
              var no = ++sl_no;
              trHTML += "<tr><td>  " + no + "  </td><td>  </td><td>  </td><td>  " + provider + "  </td><td><button type='button' class='btn btn-success pull-right' onclick = 'manual_create_request("+request_id+","+j+");'>Send Request</button></td></tr>";
            });
            trHTML += "</table>";
          }
        });
        $('#here_table').append(trHTML);      
        $("#show_providers_modal").modal('show');
     }
     else{
      $('#prov_message').html(json.error);
      $('#here_table').html("  ");
      $("#show_providers_modal").modal('show');
     }
   }

   });
 }

 function manual_create_request(request_id,provider_id){
  //alert(provider_id);return;
               var type = 're_assign';
               var request_id = request_id;
               var provider_id = provider_id;
   //          var uniq_id = $('#uniq_id').val();
   //          var service_id = $('#service_id').val();
   //          var total = $('#total').val();
   //          var first_name = $('#first_name').val();
   //          var last_name = $('#last_name').val();
   //          var email = $('#email').val();
   //          var s_latitude = $('#s_latitude').val();
   //          var s_longitude = $('#s_longitude').val();
   //          var d_latitude = $('#d_latitude').val();
   //          var d_longitude = $('#d_longitude').val();
   //          var s_address = $('#origin-input').val();
   //          var d_address = $('#destination-input').val();
   //          var request_status_type = $("#request_status_type").val();
   //          var hourly_package_id = $('#hourly_package_id').val();
   //          var airport_price_id = $('#airport_price_id').val();
   // var dataString = 'uniq_id='+uniq_id+'&service_id='+service_id+'&total='+total+'&first_name='+first_name+'&last_name='+last_name+'&email='+email+'&s_latitude='+s_latitude+'&s_longitude='+s_longitude+'&d_latitude='+d_latitude+'&d_longitude='+d_longitude+'&s_address='+s_address+'&d_address='+d_address+'&provider_id='+provider_id+'&request_status_type='+request_status_type+'&hourly_package_id='+hourly_package_id+'&airport_price_id='+airport_price_id;
     var dataString = 'type='+type+'&request_id='+request_id+'&provider_id='+provider_id;
     $.ajax({
     type: "POST",
     url : "{{route('admin.manual_create_request')}}",
     data: dataString, 
     success : function(data){
      //alert(data);return false; 
        var json = $.parseJSON(data);
       console.log(json);
        if(json.success == true){
          alert('Request sent successfully!!');
          $('#show_providers_modal').modal('hide');
         window.location.href = "{{route('admin.requests')}}";
         
        }
        if(json.success == false){
          alert(json.error);
          $('#show_providers_modal').modal('show');
        }
        
        //console.log(json);
      }
   });
 }

 </script>

@endsection
