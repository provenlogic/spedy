<?php 
  use App\Admin; 
  $is_permitted_user = Admin::find(Auth::guard('admin')->user()->id);
?>
@extends('layouts.admin')

@section('title', tr('providers'))

@section('content-header', 'Driver Report')

@section('breadcrumb')
	<li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i>{{tr('home')}}</a></li>
    <li class="active"><i class="fa fa-users"></i> {{tr('providers')}}</li>
@endsection

@section('content')

	@include('notification.notify')

	<div class="row">
		<form method="post" action="{{url('/')}}/provider_wise_report">

		<!-- <button class="btn btn-primary " onclick="$('.example1').tableExport({type:'excel',escape:'false'});" style="margin-left:10px">Export</button> -->
		<button class="btn btn-primary " type="submit" name="excel" style="margin-left:10px">Export Excel</button>
		<button class="btn btn-primary " type="submit" name="pdf" style="margin-left:10px">Export PDF</button>
		<br><br>
		
			<label>Driver:</label>
			<select name="provider" required>
				<option value="" selected >Select Driver...</option>
				@foreach($provider_list as $p_list)
				<option value="{{$p_list->id}}" @if($p_list->id == $provider_id) selected @endif>{{$p_list->first_name}} {{$p_list->last_name}} </option>
				@endforeach
			</select>
			<label>From Date</label>
			<input type="date" name="from_date" @if($fromDate)value={{$fromDate}}@endif required>
			<lable>To Date </lable>
			<input type="date" name="to_date"  @if($toDate)value={{$toDate}}@endif  required>
			<input type="submit" value="Submit"  class="btn btn-primary ">
		</form>
        <div class="col-xs-12">
          <div class="box box-info">
            <div class="box-body">

            	@if(count($providers) > 0)
            	
            		
            		
	              	<table id="example1" class="table table-bordered table-striped example1">
	              		
					<thead>

	                <tr>
	                  <th>{{ tr('id') }}</th>
	                  <!-- <th class="min">Name</th> -->
	                  <th>User</th>
	                  <!-- <th>{{ tr('total_request') }}</th>
	                  <th>{{ tr('accepted_requests') }}</th>
	                  <th>{{ tr('cancel_request') }}</th> -->
			  <th>Request Earnings</th>
			  <th>Admin Earnings</th>
				<th>Provider Earnings</th>
	                  <!-- <th>{{ tr('availability') }}</th>
	                  <th>{{ tr('status') }}</th>
	                  <th>{{ tr('action') }}</th> -->
	                  </tr>
	              </thead>
	              <tbody>
	           
	              @foreach($providers as $index => $provider)
	              <tr>
	                  <td>{{$index + 1}}</td>
	                  
	                 <td>{{$provider->first_name}} {{$provider->last_name}}</td>
	                 
			  <td>{{get_currency_value($provider->total + $provider->promo_value ? $provider->total + $provider->promo_value : 0)}}</td>
			  <td>
			  	
			  	@php
					$r = ($provider->total - $provider->provider_earnings) ? ($provider->total - $provider->provider_earnings) : 0;
					
			  	
			  	$r = abs($r);
			  	$ad_earnings = get_currency_value($r);
			  	
			  	@endphp
			  	{{$ad_earnings}}
			  </td>
			  
			  <td>{{get_currency_value($provider->provider_earnings ? $provider->provider_earnings : 0)}}</td>


	             <!--  -->
	              </tr>
	              @endforeach              		

						</tbody>
					</table>
				@else
					<h3 class="no-result">No results found</h3>
				@endif
            </div>
               	<div class="row">
               			<div class="col-md-6">
	              			<p>Driver Name:{{$driver_detail->first_name}} {{$driver_detail->last_name}} </p>
	              			<p>E-mail Id:{{$driver_detail->email}}</p>
	              		</div>	
	              		
	              		<div class="col-md-6">

	              			<p>Mobile: {{$driver_detail->mobile}}</p>
	              			
	              			<p>Total Requests:@if($t_requests){{$t_requests->total_requests}}  @endif</p>
	              		</div>
	              		
	              		<div class="col-md-6">
	              			<p>Total Admin Earnings:{{$total_admin_earnings}} </p>
	              		
	              			<p>Total Provider Earnings: {{$total_provider_earnings}}</p>
	              		</div>
	              		<div class="col-md-6">	              
	              		
	              			<p>Total Accepted Requests: {{$total_accepted_requests}}</p>
	              			@if($t_requests)
	              		<?php $total_canceled_requests =  $t_requests->total_requests-$total_accepted_requests;  ?>
	              			<p>Total Cancelled Requests:{{$total_canceled_requests}}</p>
	              			@else
	              			<p>Total Cancelled Requests:</p>
	              			@endif
	              		</div>
	              		
	              </div>
          </div>
        </div>
    </div>

@endsection