@extends('layouts.admin')

@if(isset($name))
  @section('title', 'Withdraw Amount')
@else
  @section('title', 'Withdraw Amount')
@endif


@section('content-header', 'Withdraw Amount')

@section('breadcrumb')
    <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i>Home</a></li>
    <li><a href="{{route('admin.service.types')}}"><i class="fa fa-user"></i> {{tr('service_types')}}</a></li>
    <li class="active">Withdraw Amount</li>
@endsection

@section('content')

@include('notification.notify')

<style>
  @media (min-width: 768px)
  {
    .modal-dialog {
    width: 400px;
    margin: 30px auto;
  }
  }

</style>
<div class="row">

  <div class="col-md-12">

      <div class="box box-info">
          <div class="box-header">
        <div class="map_content">
            <p class="lead ">
             * Use this screen to Withdraw amount from provider
            </p>
            <p>
              
            </p>

            <p>
              <b>Provider Details</b>
            </p>
            <p>
              {{ $provider->first_name }} {{ $provider->last_name }}
            </p>
            <p>
              {{ $provider->email }}
            </p>
            <p>
              {{ $provider->mobile }}
            </p>
            <p>
              Current Balance : $ {{ $current_balance }}
            </p>
             
          </div>
          </div>
          <!-- <div class="box-header">
            @if(isset($name))
            {{ tr('edit_service') }}
            @else
              {{ tr('create_service') }}
            @endif
          </div> -->
          <div class="panel-heading border">

          </div>

          <form class="form-horizontal bordered-group" action="{{route('admin.add.withdraw.process')}}" method="POST" enctype="multipart/form-data" role="form">
            <div class="form-group">
              <label class="col-sm-2 control-label">Withdraw Amount</label>
              <div class="col-sm-8">
                <input type="text" name="withdraw_amount"  required class="form-control">
              </div>
            </div>
            
            
            <input type="hidden" name="id" value="{{ $provider->id }}" />

            <div class="box-footer">
                <a href="{{ route('admin.service.types' ) }}" class="btn btn-danger">{{tr('cancel')}}</a>
                <button type="submit" class="btn btn-success pull-right">{{tr('submit')}}</button>
            </div>

          </form>

      </div>

  </div>

</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title text-center" id="myModalLabel">Add a Vehicle type</h4>
      </div>
      <div class="modal-body">
        <img src="../images/Vehicle-Type.png" alt="img" width="100%">
      </div>
<!--       <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div> -->
    </div>
  </div>
</div>

@endsection
