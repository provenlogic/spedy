<?php 
    use App\Admin; 
    $is_permitted_user = Admin::find(Auth::guard('admin')->user()->id);
?>

@extends('layouts.admin')
@section('title', tr('providers'))
@section('content-header', 'View all drivers who are online')
@section('breadcrumb')
<li>
    <a href="{{route('admin.dashboard')}}">
        <i class="fa fa-dashboard"></i>{{tr('home')}}
    </a>
</li>
<li class="active">
    <i class="fa fa-users"></i> {{tr('providers')}}
</li>
@endsection
@section('content')
@include('notification.notify')
<div class="row">


    <div class="col-xs-12">

            <a class="btn btn-primary " href="{{url('/')}}/admin/providersExcelOnline" style="margin-left:10px">Export Excel</a>
            <a class="btn btn-primary " href="{{url('/')}}/admin/providersPdfOnline" style="margin-left:10px">Export PDF</a>
            <!-- <button class="btn btn-primary " onclick="$('.example1').tableExport({type:'excel',escape:'false'});" style="margin-left:10px">Export</button> -->
      <br><br>
      
        <div class="box box-info">
            <div class="box-body table-responsive">
                @if(count($providers) > 0)
                <?php $total_ad_earnings=0; ?>
                <table id="example1" class="table table-bordered table-striped example1">
                    <thead>
                        <tr >
                            <th>{{ tr('id') }}</th>
                            <th class="min">Name</th>
                            <th>Vehicle Category</th>
                            <th>Current Location</th>
                            <th>{{ tr('total_request') }}</th>
                            <th>{{ tr('accepted_requests') }}</th>
                            <th>{{ tr('cancel_request') }}</th>
                            
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($providers as $index => $provider)
                        <tr class="providers" data-latitude="{{$provider->latitude}}" data-longitude="{{$provider->longitude}}">
                            <td>{{$index + 1}}</td>
                            <td>{{$provider->first_name}} {{$provider->last_name}}</td>
                            <td>{{$provider->name}}</td> 
                            <td class="address"></td>    
                            <td>{{$provider->total_requests}}</td>
                            <td>{{$provider->accepted_requests}}</td>
                            <td>{{$provider->total_requests -$provider->accepted_requests }}</td>                     
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                @else
                <h3 class="no-result">No results found</h3>
                @endif
            </div>
        </div>
    </div>
</div>
<script>

    var gapi = 'https://maps.googleapis.com/maps/api/geocode/json?key={{env('GOOGLE_MAP_KEY_FOR_NIKOLA')}}&latlng=';

    function loadAddress()
    {

        $(".providers .address").text('Loading...')
        $(".providers").each((index, item)=>{

            let latitude = $(item).data('latitude')
            let longitude = $(item).data('longitude')
            let url = gapi+`${latitude},${longitude}`;
            let addressCell = $(item).find('.address')
            

            $.get(url, function(response){
                //console.log(response.results[5]['formatted_address'])
                try {
                    addressCell.text(response.results[5]['formatted_address'])
                } catch(e) {
                    addressCell.text('Not Found')
                }
                
            })


         })

    }


    setTimeout(()=>{
        window.location.reload()
    }, 1000 * 60 * 5)


    window.onload = ()=>{
        
        loadAddress();
        $("#example1").on('draw.dt', function(){ 
            loadAddress();
        })


    }


</script>
@endsection