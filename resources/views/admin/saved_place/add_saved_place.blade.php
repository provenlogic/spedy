@extends('layouts.admin')

@if(isset($name))
  @section('title', tr('edit_airport_pricing'))
@else
  @section('title', 'Pricing Setup')
@endif


@section('content-header', 'Add Saved Place Price')

@section('breadcrumb')
    <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i>Home</a></li>
    <li><a href="{{route('admin.airport_pricings')}}"><i class="fa fa-user"></i> {{tr('airport_pricings')}}</a></li>
    <li class="active">{{tr('airport_pricings')}}</li>
@endsection

@section('content')

@include('notification.notify')

<div class="row">

  <div class="col-md-12">

      <div class="box box-info">

    
          <div class="panel-heading border">

          </div>

          <form class="form-horizontal bordered-group" action="{{route('admin.add_saved.process')}}" method="POST" enctype="multipart/form-data" role="form">
            <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key={{ $key }}&sensor=false&libraries=places"></script>
            <script type="text/javascript">
                google.maps.event.addDomListener(window, 'load', function () {
                    var places = new google.maps.places.Autocomplete(document.getElementById('txtPlaces'));
                    google.maps.event.addListener(places, 'place_changed', function () {
                        var place = places.getPlace();
                        var address = place.formatted_address;
                        var latitude = place.geometry.location.lat();
                        var longitude = place.geometry.location.lng();
                        var mesg = "Address: " + address;
                        mesg += "\nLatitude: " + latitude;
                        mesg += "\nLongitude: " + longitude;
                        document.getElementById("latitude").value = latitude;
                        document.getElementById("longitude").value = longitude;
                        document.getElementById("address").value = address;

                        console.log(latitude,longitude, address);
                    });
                });
            </script>

            <input type="hidden" value="" name ="address" id="address" />
            <input type="hidden" value="" name ="latitude" id="latitude" />
            <input type="hidden" value="" name ="longitude" id="longitude" />
            <input type="hidden" name="id" value="@if(isset($saved_place)) {{$saved_place->id}} @endif" />



            <div class="form-group">
              <label class="col-sm-2 control-label">Place</label>
              <div class="col-sm-8">
                <input id="txtPlaces" placeholder="" type="text" name="name" value="{{ isset($saved_place->address) ? $saved_place->address : '' }}" required class="form-control">
              </div>
            </div>

            

            <div class="form-group">
              <label class="col-sm-2 control-label">Vehicle Type</label>
              <div class="col-sm-8">
                <select name="service_type" required class="form-control">
                    <option value="{{isset($service_type->name) ? $service_type->id: 1}}">{{ isset($service_type->name) ? $service_type->name : "Select Vehicle Type" }}</option>
                    @foreach($service_types as $service_type)
  
                      <option value="{{$service_type->id}}">{{$service_type->name}}</option>
                    @endforeach
                </select>
                
              </div>
            </div>
            
            <div class="form-group">
              <label class="col-sm-2 control-label">{{ tr('price') }}</label>
              <div class="col-sm-8">
                <input placeholder="Eg: 100" type="text" name="price" value="{{ isset($saved_place->price) ? $saved_place->price : '' }}" required class="form-control">
              </div>
            </div>

        

            
            <div class="box-footer">
                <button type="reset" class="btn btn-danger">{{tr('cancel')}}</button>
                <button type="submit" class="btn btn-success pull-right">{{tr('submit')}}</button>
            </div>

          </form>

      </div>

  </div>

</div>


@endsection
