<?php 
  use App\Admin; 
  $is_permitted_user = Admin::find(Auth::guard('admin')->user()->id);
?>
@extends('layouts.admin')

@section('title', 'Saved Places')

@section('content-header','Saved Places')

@section('breadcrumb')
    <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i>{{tr('home')}}</a></li>
    <li class="active"><i class="fa fa-user"></i> {{tr('airport_pricings')}}</li>
@endsection

@section('content')

  @include('notification.notify')


<div class="row">
      <div class="col-xs-12">
        <div class="box box-info">
          <div class="box-body">
                  <div class="box-header">
                
            @if(count($saved_places) > 0)

          <table id="example1" class="table table-bordered table-striped">

            <thead>
              <tr>
                <th>{{ tr('id') }}</th>
                <th class="min">Address</th>
                <th class="min">Service Type</th>
                <th class="min">Price</th>
                <th>{{ tr('action') }}</th>

             </tr>
            </thead>
            <tbody>
            @foreach($saved_places as $index => $saved_place)
            <tr>
                <td>{{$index + 1 }}</td>
                <td>{{$saved_place->address}}</td>
                <td>{{$saved_place->service_type->name}}</td>
                <td>{{$saved_place->price}}</td>
                <td class="btn-left">
                    <div class="input-group-btn">
                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">{{ tr('action') }}
                          <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu">
                            <li>
                              <a href="{{route('admin.edit.saved', array('id' => $saved_place->id, 'service_id' => $saved_place->service_type->id ))}}">{{ tr('edit') }}</a>
                            </li>

                          <li>
                            <a onclick="return confirm('{{ tr('delete_confirmation') }}')" href="{{route('admin.delete.saved', array('id' => $saved_place->id))}}">{{ tr('delete') }}</a>
                          </li>
                         </ul>
                      </div>
                </td>
                
         
            </tr>
            @endforeach
          </tbody>
        </table>
      @else
        <h3 class="no-result">{{tr('no_data_found')}}</h3>
      @endif
          </div>
        </div>
      </div>
  </div>


@endsection
