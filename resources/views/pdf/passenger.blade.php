<style type="text/css">
	
	table {
    border-collapse: collapse;
}

table, th, td {
    border: 1px solid black;
}
</style>


<div class="row" class="tbl_grid_report" >

<!--show the data report-->
<table class="table" cellspacing="5" cellpadding="10"  style="font-size:11px;" >



<tr valign="top" align="center">
	<th align="left">Id</th>
	<th align="left">Name</th>
	<th align="left">Email</th>
	<th align="left">Mobile</th>
	<th align="left">Pickup Location	</th>


</tr>

	@foreach($excel as $i => $user)
	<tr>
									<td>{{$i+1}}</td>
							      	<td>{{$user->first_name}} {{$user->last_name}}</td>
							      	<td>{{$user->email}}</td>
                      				<td>{{$user->mobile}}</td>
							      	<td>{{$user->address}}</td>
	
		</tr>
	@endforeach

</table>

</div>