
<style type="text/css">
	
	table {
    border-collapse: collapse;
}

table, th, td {
    border: 1px solid black;
}
</style>


<div class="row" class="tbl_grid_report" >

<!--show the data report-->
<table  class="table table-bordered table-striped example1">
	              		
					<thead>

	                <tr>
	                  <th>{{ tr('id') }}</th>
	                  <!-- <th class="min">Name</th> -->
	                  <th>User</th>
	                  <!-- <th>{{ tr('total_request') }}</th>
	                  <th>{{ tr('accepted_requests') }}</th>
	                  <th>{{ tr('cancel_request') }}</th> -->
			  <th>Request Earnings</th>
			  <th>Admin Earnings</th>
			  <th>Provider Earnings</th>
	                  <!-- <th>{{ tr('availability') }}</th>
	                  <th>{{ tr('status') }}</th>
	                  <th>{{ tr('action') }}</th> -->
	                  </tr>
	              </thead>
	              <tbody>
	              	<tr>
	              			<td>Driver Name: </td>
	              			<td>{{$driver_detail->first_name}} {{$driver_detail->last_name}}</td>
	              			<td>E-mail Id:</td>
	              			<td>{{$driver_detail->email}}</td>
	              			<td></td>
	              		</tr>
	              	
	              		<tr>
	              			<td>Mobile: </td>
	              			<td>{{$driver_detail->mobile}}</td>
	              			<td>Total Requests:</td>
	              			<td>@if($t_requests){{$t_requests->total_requests}} @else 0 @endif</td>
	              			<td></td>
	              		</tr>
	              	  	 <tr>
	              			<td>Total Admin Earnings: </td>
	              			<td>{{$total_admin_earnings}}</td>
	              			<td>Total Provider Earnings:</td>
	              			<td>{{$total_provider_earnings}}</td>
	              			<td></td>
	              		</tr>

	              
	              		<tr>
	              			<td>Total Accepted Requests: </td>
	              			<td>{{$total_accepted_requests}}</td>
	              			<td>Total Cancelled Requests:</td>
	              			@if($t_requests)
	              			<?php $total_canceled_requests = $t_requests->total_requests-$total_accepted_requests; ?> @endif
	              			<td>@if($t_requests) {{$total_canceled_requests}} @else 0 @endif</td>
	              			<td></td>
	              		</tr>
	              @foreach($providers as $index => $provider)
	              <tr>
	                  <td>{{$index + 1}}</td>
	                  
	                 <td>{{$provider->first_name}} {{$provider->last_name}}</td>
	                 
			  <td>{{get_currency_value($provider->total + $provider->promo_value ? $provider->total + $provider->promo_value : 0)}}</td>
			  <td>
			  	
			  	@php
			  	$r = ($provider->total - $provider->provider_earnings) ? ($provider->total - $provider->provider_earnings) : 0;
			  	
			  	$r = abs($r);
			  	$ad_earnings = get_currency_value($r);
			  	
			  	@endphp
			  	{{$ad_earnings}}
			  </td>
			  
			  <td>{{get_currency_value($provider->provider_earnings ? $provider->provider_earnings : 0)}}</td>
	                 

	             <!--  -->
	              </tr>
	              @endforeach              		

						</tbody>
					</table>

</div>