<style type="text/css">
	
	table {
    border-collapse: collapse;
}

table, th, td {
    border: 1px solid black;
}
</style>


<div class="row" class="tbl_grid_report" >

<!--show the data report-->
<table class="table" cellspacing="5" cellpadding="10"  style="font-size:11px;" >



<tr valign="top" align="center">
    <th align="left">Id</th>
    <th style="text-align:left">Driver Name</th>
	<th align="left">Date and Time</th>
    <th align="left">Status</th>
    <th style="text-align:left">Price</th>




</tr>

	@foreach($excel as $i => $ride)
	<tr>
                                    <td>{{$i+1}}</td>
                                    <td>{{$ride->first_name}} {{$ride->last_name}}</td>
                                    <td>{{$ride->date}}</td>
                                    <td>@if($ride->status == 0)
                                            New
                                      @elseif($ride->status == 1)
                                            Going to Pickup
                                      @elseif($ride->status == 2)
                                            Trip Started
                                        @elseif($ride->status == 3)
                                            Trip ended
                                        @endif
                                    </td>

                                    <td>
                                        {{$ride->total}}
                                    </td>
                    
							      	
	
		</tr>
	@endforeach

</table>

</div>