<div class="row" class="tbl_grid_report" >
    <!--show the data report-->
    <table class="table" cellspacing="5" cellpadding="10"  style="font-size:11px;" >
        <tr valign="top" style="text-align:center">
            <th style="text-align:left">Id</th>
            <th style="text-align:left">Name</th>
            <th style="text-align:left">Email</th>
            <th style="text-align:left">Mobile</th>
        </tr>
        @foreach($excel as $i => $user)
        <tr>
            <td>{{$i+1}}</td>
            <td>{{$user->first_name}} {{$user->last_name}}</td>
            <td>{{$user->email}}</td>
            <td>{{$user->mobile}}</td>
            <td>{{$user->address}}</td>
        </tr>
        @endforeach
    </table>
</div>