<div class="row" class="tbl_grid_report" >
        <!--show the data report-->
        <table class="table" cellspacing="5" cellpadding="10"  style="font-size:11px;" >
            <tr valign="top" style="text-align:center">
                <th style="text-align:left">Id</th>
                <th style="text-align:left">Driver Name</th>
                <th style="text-align:left">Passenger Name</th>
                <th style="text-align:left">Date and Time</th>
                <th style="text-align:left">Status</th>
                <th style="text-align:left">Price</th>

            </tr>
            @foreach($excel as $i => $ride)
            <tr>
                <td>{{$i+1}}</td>
                <td>{{$ride->provider_first_name}} {{$ride->provider_last_name}}</td>
                <td>{{$ride->user_first_name}} {{$ride->user_last_name}}</td>
                <td>{{$ride->date}}</td>
                <td>@if($ride->status == 0)
                        New
                  @elseif($ride->status == 1)
                        Waiting
                  @elseif($ride->status == 2)

                    @if($ride->provider_status == 0)
                        Provider Not Found
                    @elseif($ride->provider_status == 1)
                        Provider Accepted
                    @elseif($ride->provider_status == 2)
                        Provider Started
                    @elseif($ride->provider_status == 3)
                        Provider Arrived
                    @elseif($ride->provider_status == 4)
                        Service Started
                    @elseif($ride->provider_status == 5)
                        Service Completed
                    @elseif($ride->provider_status == 6)
                        Provider Rated
                    @endif

                    @elseif($ride->status == 3)

                          Payment Pending
                    @elseif($ride->status == 4)

                          Request Rating
                    @elseif($ride->status == 5)

                          Request Completed
                    @elseif($ride->status == 6)

                          Request Cancelled
                    @elseif($ride->status == 7)

                          Provider Not Available
                    @endif
                </td>

                <td>@if($ride->status == 6)
                    {{get_currency_value($ride->cancellation_fine ? $ride->cancellation_fine : 0)}}
                    @else
                    {{get_currency_value($ride->amount ? $ride->amount : 0)}}
                    @endif
                </td>

            
            </tr>
            @endforeach
        </table>
    </div>