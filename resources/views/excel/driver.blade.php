<div class="row" class="tbl_grid_report" >
    <!--show the data report-->
    <?php $total_ad_earnings=0; ?>
    <table class="table table-bordered table-striped example1">
        <thead>
            <tr>
                <th>{{ tr('id') }}</th>
                <th class="min">Name</th>
                <th>{{ tr('email') }}</th>
                <th>Mobile</th>
                <th>{{ tr('total_request') }}</th>
                <th>{{ tr('accepted_requests') }}</th>
                <th>{{ tr('cancel_request') }}</th>
                <th>Request Earnings</th>
                <th>Admin Earnings</th>
                <th>Provider Earnings</th>
                <th>{{ tr('availability') }}</th>
                <th>{{ tr('status') }}</th>
                <th>Wallet Balance</th>

            </tr>
        </thead>
        <tbody>
            @foreach($providers as $index => $provider)
            <tr>
                <td>{{$index + 1}}</td>
                <td>{{$provider->first_name}} {{$provider->last_name}}</td>
                <td>{{$provider->email}}</td>
                <td>{{$provider->mobile}}</td>
                <td>{{$provider->total_requests}}</td>
                <td>{{$provider->accepted_requests}}</td>
                <td>{{$provider->total_requests -$provider->accepted_requests }}</td>
                <td>{{get_currency_value($provider->total_request_earnings + $provider->total_promo_value ? $provider->total_request_earnings + $provider->total_promo_value : 0)}}</td>
                <td>
                    @php
                    $r = ($provider->total_request_earnings - $provider->total_provider_earnings) ? ($provider->total_request_earnings - $provider->total_provider_earnings) : 0;
                    $r = abs($r);
                    $ad_earnings = get_currency_value($r);
                    $total_ad_earnings = $total_ad_earnings + $r;
                    @endphp
                    {{$ad_earnings}}
                </td>
                <td>{{get_currency_value($provider->total_provider_earnings ? $provider->total_provider_earnings : 0)}}</td>
                <td>@if($provider->is_available==1) <span class="label label-primary">{{ tr('yes')}}</span> @else <span class="label label-warning">N/A</span> @endif</td>
                <td>@if($provider->is_approved==1) <span class="label label-success">{{ tr('approved') }}</span> @else <span class="label label-danger">{{ tr('unapproved') }}</span> @endif</td>
                <td>{{$provider->wallet_balance}}</td>
            </tr>
            @endforeach
            

			<tr>
                <td></td>
            </tr>
			<tr>
                <td>Total Requests: </td>
                <td>Total Admin earnings:</td>
                <td>Total Provider earnings: </td>
            </tr>
			<tr>
                <td>{{$total_requests}}</td>
                <td>{{$total_ad_earnings}}</td>
                <td>{{$total_provider_earnings}}</td>
            </tr>

        </tbody>
    </table>
</div>