<div class="row" class="tbl_grid_report" >
        <!--show the data report-->
        <?php $total_ad_earnings=0; ?>
        <table class="table table-bordered table-striped example1">
            <thead>
                <tr>
                    <th>{{ tr('id') }}</th>
                    <th class="min">Name</th>
                    <th>{{ tr('email') }}</th>
                    <th>Mobile</th>
                    <th>{{ tr('total_request') }}</th>
                    <th>{{ tr('accepted_requests') }}</th>
                    <th>{{ tr('cancel_request') }}</th>
                    <th> Vehicle Category </th>
    
                </tr>
            </thead>
            <tbody>
                @foreach($providers as $index => $provider)
                <tr>
                    <td>{{$index + 1}}</td>
                    <td>{{$provider->first_name}} {{$provider->last_name}}</td>
                    <td>{{$provider->email}}</td>
                    <td>{{$provider->mobile}}</td>
                    <td>{{$provider->total_requests}}</td>
                    <td>{{$provider->accepted_requests}}</td>
                    <td>{{$provider->total_requests -$provider->accepted_requests }}</td>
                    
                    <td>{{$provider->name}}</td>
                </tr>
                @endforeach
                
    
                <tr>
                    <td></td>
                </tr>
                <tr>
                    <td>Total Requests: </td>
                </tr>
                <tr>
                    <td>{{$total_requests}}</td>
                </tr>
    
            </tbody>
        </table>
    </div>